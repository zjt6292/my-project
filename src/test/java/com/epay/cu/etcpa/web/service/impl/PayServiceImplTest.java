package com.epay.cu.etcpa.web.service.impl;

import com.epay.cu.etcpa.web.model.OrderInfo;
import com.epay.cu.etcpa.web.model.PayDetail;
import com.epay.cu.etcpa.web.service.PayService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author guoyankui
 * @date 2018/6/21 10:50 AM
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PayServiceImplTest {

    @Autowired
    private PayService payService;

    @Test
    @Transactional
    public void pay() throws Exception {
        String payJournalNo = "1234567";
        String userId = "100902123";
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setMerOrderNo("20180515009912");
        orderInfo.setGoodsId("43200329");
        orderInfo.setGoodsName("手机米");
        orderInfo.setMerNo("3009832234");
        orderInfo.setAmount(8009L);
        List<PayDetail> payDetails = payService.pay(payJournalNo, orderInfo, userId);
        Assert.assertEquals(1, payDetails.size());
    }

    @Test
    public void getOrder() throws Exception {
        String merOrderNo = "20180515009901";
        OrderInfo orderInfo = payService.getOrder(merOrderNo);
        Assert.assertEquals(merOrderNo, orderInfo.getMerOrderNo());
    }


}
