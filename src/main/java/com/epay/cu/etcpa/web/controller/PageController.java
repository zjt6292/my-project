package com.epay.cu.etcpa.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author guoyankui
 * @date 2018/6/19 7:07 PM
 * @description 返回页面的controller类
 */
@Controller
@RequestMapping("/v1")
public class PageController {

    @GetMapping("/index")
    public String index(){
        return "index";
    }
}
