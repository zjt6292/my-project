package com.epay.cu.etcpa.web.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author guoyankui
 * @date 2018/6/19 2:27 PM
 * @description 支付请求信息
 */
@ApiModel
public class PayRequest {

    @ApiModelProperty(value = "支付流水号")
    private String payJournalNo;
    @ApiModelProperty(value = "订单信息")
    private OrderInfo orderInfo;
    @ApiModelProperty(value = "用户号")
    private String userId;

    public String getPayJournalNo() {
        return payJournalNo;
    }

    public void setPayJournalNo(String payJournalNo) {
        this.payJournalNo = payJournalNo;
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"payJournalNo\":\"")
                .append(payJournalNo).append('\"');
        sb.append(",\"orderInfo\":")
                .append(orderInfo);
        sb.append(",\"userId\":\"")
                .append(userId).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
