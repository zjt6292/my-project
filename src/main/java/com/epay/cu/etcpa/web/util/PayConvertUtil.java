package com.epay.cu.etcpa.web.util;

import com.epay.cu.etcpa.web.model.PayDetail;
import com.epay.cu.etcpa.web.model.PayResponse;

import java.util.List;

/**
 * @author guoyankui
 * @date 2018/6/19 3:34 PM
 * @description
 */
public class PayConvertUtil {
    public static PayResponse wrapPayResponse(String payJournalNo, String userId, List<PayDetail> payDetailList){
        PayResponse payResponse = new PayResponse();
        payResponse.setPayJournalNo(payJournalNo);
        payResponse.setUserId(userId);
        payResponse.setPayDetails(payDetailList);
        return payResponse;
    }
}
