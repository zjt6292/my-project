package com.epay.cu.etcpa.web.entity;

import java.io.Serializable;

public class PayJournal implements Serializable {
    private String payJournalNo;

    private String merOrderNo;

    private Long amount;

    private String status;

    private static final long serialVersionUID = 1L;

    public String getPayJournalNo() {
        return payJournalNo;
    }

    public void setPayJournalNo(String payJournalNo) {
        this.payJournalNo = payJournalNo;
    }

    public String getMerOrderNo() {
        return merOrderNo;
    }

    public void setMerOrderNo(String merOrderNo) {
        this.merOrderNo = merOrderNo;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PayJournal other = (PayJournal) that;
        return (this.getPayJournalNo() == null ? other.getPayJournalNo() == null : this.getPayJournalNo().equals(other.getPayJournalNo()))
                && (this.getMerOrderNo() == null ? other.getMerOrderNo() == null : this.getMerOrderNo().equals(other.getMerOrderNo()))
                && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
                && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPayJournalNo() == null) ? 0 : getPayJournalNo().hashCode());
        result = prime * result + ((getMerOrderNo() == null) ? 0 : getMerOrderNo().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", payJournalNo=").append(payJournalNo);
        sb.append(", merOrderNo=").append(merOrderNo);
        sb.append(", amount=").append(amount);
        sb.append(", status=").append(status);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}