package com.epay.cu.etcpa.web.mapper;

import com.epay.cu.etcpa.web.entity.PayJournal;

public interface PayJournalMapper {
    int deleteByPrimaryKey(String payJournalNo);

    int insert(PayJournal record);

    int insertSelective(PayJournal record);

    PayJournal selectByPrimaryKey(String payJournalNo);

    int updateByPrimaryKeySelective(PayJournal record);

    int updateByPrimaryKey(PayJournal record);
}