package com.epay.cu.etcpa.web.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author guoyankui
 * @date 2018/6/19 2:27 PM
 * @description 订单信息
 */
@ApiModel
public class OrderInfo {

    @ApiModelProperty(value = "商户订单号")
    private String merOrderNo;
    @ApiModelProperty(value = "商户号")
    private String merNo;
    @ApiModelProperty(value = "商品编号")
    private String goodsId;
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    @ApiModelProperty(value = "订单金额")
    private Long amount;

    public String getMerOrderNo() {
        return merOrderNo;
    }

    public void setMerOrderNo(String merOrderNo) {
        this.merOrderNo = merOrderNo;
    }

    public String getMerNo() {
        return merNo;
    }

    public void setMerNo(String merNo) {
        this.merNo = merNo;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"merOrderNo\":\"")
                .append(merOrderNo).append('\"');
        sb.append(",\"merNo\":\"")
                .append(merNo).append('\"');
        sb.append(",\"goodsId\":\"")
                .append(goodsId).append('\"');
        sb.append(",\"goodsName\":\"")
                .append(goodsName).append('\"');
        sb.append(",\"amount\":")
                .append(amount);
        sb.append('}');
        return sb.toString();
    }
}
