package com.epay.cu.etcpa.web.configuration;

import com.epay.ts.piss.common.util.EnvironmentUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author guoyankui
 * @date 2018/6/19 2:37 PM
 * @description swagger ui的文档
 */
@Configuration
public class Swagger2Configuration {

    @Value("${spring.application.name}")
    private String systemName;
    @Value("${info.app.version}")
    private String appVersion;

    @Bean
    public Docket createRestApi() {
        String pathMapping="";
        String appEnvType= EnvironmentUtil.getEnv("ENV");
        String superApplicationName=systemName.substring(0,systemName.indexOf("-",systemName.indexOf("-")+1));
        if("sit".equalsIgnoreCase(appEnvType))
        {
            pathMapping="/"+systemName+"."+superApplicationName+".sit.epay";
        }
        if(("uat1").equalsIgnoreCase(appEnvType)||("uat_1").equalsIgnoreCase(appEnvType))
        {
            pathMapping="/"+systemName+"."+superApplicationName+".uat1.epay";
        }
        if(("pet").equalsIgnoreCase(appEnvType))
        {
            pathMapping="/"+systemName+"."+superApplicationName+".pet.epay";
        }
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping(pathMapping)
                .groupName(systemName)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.epay.cu.etcpa.web.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(systemName+"的接口文档")
                .description("基于 swagger 2 的自动化文档")
                .contact(new Contact("guoyankui", "http://gitcode.unicompayment.com/rabbitGYK", "guoyk8@chinaunicom.cn"))
                .version(appVersion)
                .license("Galaxy License, Version 2.0")
                .licenseUrl("http://gitcode.unicompayment.com/Galaxy")
                .build();
    }
}
