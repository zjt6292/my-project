package com.epay.cu.etcpa.web.service;

import com.epay.cu.etcpa.web.model.OrderInfo;
import com.epay.cu.etcpa.web.model.PayDetail;

import java.util.List;

/**
 * @author guoyankui
 * @date 2018/6/19 3:45 PM
 * @description
 */
public interface PayService {

    /**
     * 默认支付（支持多个支付工具：余额 > 快捷）
     * @param payJournalNo 支付流水号
     * @param orderInfo 订单信息
     * @param userId 用户号
     * @return 支付详情
     */
    List<PayDetail> pay(String payJournalNo, OrderInfo orderInfo, String userId);

    /**
     * 根据商户订单号获取订单信息
     * @param merOrderNo 商户订单号
     * @return
     */
    OrderInfo getOrder(String merOrderNo);
}
