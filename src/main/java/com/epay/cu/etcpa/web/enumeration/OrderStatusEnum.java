package com.epay.cu.etcpa.web.enumeration;

/**
 * @author guoyankui
 * @date 2018/6/20 3:47 PM
 * @description
 */
public enum OrderStatusEnum {

    INIT("00"),
    SUCCESS("01"),
    FAIL("02"),
    UNKNOWN("03");

    private final String code;

    OrderStatusEnum(String code){
        this.code = code;
    }

    public String code() {
        return code;
    }

}
