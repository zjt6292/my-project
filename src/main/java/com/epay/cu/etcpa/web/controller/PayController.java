package com.epay.cu.etcpa.web.controller;

import com.epay.cu.etcpa.web.model.OrderInfo;
import com.epay.cu.etcpa.web.model.PayDetail;
import com.epay.cu.etcpa.web.model.PayRequest;
import com.epay.cu.etcpa.web.model.PayResponse;
import com.epay.cu.etcpa.web.service.PayService;
import com.epay.cu.etcpa.web.util.PayConvertUtil;
import com.epay.ts.piss.common.entity.ResultEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author guoyankui
 * @date 2018/6/19 2:37 PM
 * @description
 */
@Api(value = "支付接口controller", tags = {"支付接口"})
@RestController
@RequestMapping("/v1")
public class PayController {

    @Autowired
    private PayService payService;

    @ApiOperation(value = "默认支付接口")
    @ApiImplicitParams(@ApiImplicitParam(paramType = "body", dataType = "PayRequest", name = "payRequest", value = "支付请求"))
    @PostMapping(value = "/pay", produces = "application/json")
    public ResultEntity<PayResponse> pay(@RequestBody PayRequest payRequest) {
        List<PayDetail> payDetailList = payService.pay(payRequest.getPayJournalNo(), payRequest.getOrderInfo(), payRequest.getUserId());
        PayResponse payResponse = PayConvertUtil.wrapPayResponse(payRequest.getPayJournalNo(), payRequest.getUserId(), payDetailList);
        ResultEntity<PayResponse> resultEntity = new ResultEntity<>(payResponse);
        return resultEntity;
    }

    @ApiOperation(value = "获取订单详细信息")
    @ApiImplicitParams(@ApiImplicitParam(paramType = "query", dataType = "String", name = "merOrderNo", value = "商户订单号"))
    @GetMapping(value = "/order", produces = "application/json")
    public ResultEntity<OrderInfo> get(@RequestParam String merOrderNo) {
        OrderInfo orderInfo = payService.getOrder(merOrderNo);
        return new ResultEntity<>(orderInfo);
    }
}
