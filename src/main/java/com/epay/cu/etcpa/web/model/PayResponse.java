package com.epay.cu.etcpa.web.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author guoyankui
 * @date 2018/6/19 2:27 PM
 * @description 支付响应信息
 */
@ApiModel
public class PayResponse {
    @ApiModelProperty(value = "支付流水号")
    private String payJournalNo;
    @ApiModelProperty(value = "支付详细信息")
    private List<PayDetail> payDetails;
    @ApiModelProperty(value = "用户号")
    private String userId;

    public String getPayJournalNo() {
        return payJournalNo;
    }

    public void setPayJournalNo(String payJournalNo) {
        this.payJournalNo = payJournalNo;
    }

    public List<PayDetail> getPayDetails() {
        return payDetails;
    }

    public void setPayDetails(List<PayDetail> payDetails) {
        this.payDetails = payDetails;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"payJournalNo\":\"")
                .append(payJournalNo).append('\"');
        sb.append(",\"payDetails\":")
                .append(payDetails);
        sb.append(",\"userId\":\"")
                .append(userId).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
