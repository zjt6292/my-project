package com.epay.cu.etcpa.web;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import com.epay.ts.piss.basic.springboot.GracefulShutdownTomcat;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author guoyankui
 * @date 2018/6/19 2:37 PM
 * @description 启动类
 */
@SpringBootApplication
@EnableApolloConfig({"application","cu-etcpa-web"})
@EnableSwagger2
@MapperScan("com.epay.cu.etcpa.web.mapper")
public class WebApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class);
    }

    @Autowired
    private GracefulShutdownTomcat gracefulShutdownTomcat;

    @Bean
    public ServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
        tomcat.addConnectorCustomizers(gracefulShutdownTomcat);
        return tomcat;
    }

}
