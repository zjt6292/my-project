package com.epay.cu.etcpa.web.mapper;

import com.epay.cu.etcpa.web.entity.OrderInfo;

public interface OrderInfoMapper {
    int deleteByPrimaryKey(String merOrderNo);

    int insert(OrderInfo record);

    int insertSelective(OrderInfo record);

    OrderInfo selectByPrimaryKey(String merOrderNo);

    int updateByPrimaryKeySelective(OrderInfo record);

    int updateByPrimaryKey(OrderInfo record);
}