package com.epay.cu.etcpa.web.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author guoyankui
 * @date 2018/6/19 2:27 PM
 * @description 支付详细信息
 */
@ApiModel
public class PayDetail {

    @ApiModelProperty(value = "支付工具")
    private String payTool;
    @ApiModelProperty(value = "资金来源")
    private String fundSource;
    @ApiModelProperty(value = "支付金额")
    private Long amount;

    public String getPayTool() {
        return payTool;
    }

    public void setPayTool(String payTool) {
        this.payTool = payTool;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"payTool\":\"")
                .append(payTool).append('\"');
        sb.append(",\"fundSource\":\"")
                .append(fundSource).append('\"');
        sb.append(",\"amount\":")
                .append(amount);
        sb.append('}');
        return sb.toString();
    }
}
