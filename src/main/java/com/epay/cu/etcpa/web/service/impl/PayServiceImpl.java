package com.epay.cu.etcpa.web.service.impl;

import com.epay.cu.etcpa.web.enumeration.OrderStatusEnum;
import com.epay.cu.etcpa.web.enumeration.PayJournalStatusEnum;
import com.epay.cu.etcpa.web.entity.PayJournal;
import com.epay.cu.etcpa.web.mapper.OrderInfoMapper;
import com.epay.cu.etcpa.web.mapper.PayJournalMapper;
import com.epay.cu.etcpa.web.service.PayService;
import com.epay.cu.etcpa.web.model.OrderInfo;
import com.epay.cu.etcpa.web.model.PayDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author guoyankui
 * @date 2018/6/19 3:54 PM
 * @description
 */
@Service
public class PayServiceImpl implements PayService {

    private static final Logger logger = LoggerFactory.getLogger(PayServiceImpl.class);

    @Autowired
    private OrderInfoMapper orderInfoMapper;
    @Autowired
    private PayJournalMapper payJournalMapper;

    // 取值来自配置中心
    //@Value("${unipay.url}")
    private String unipayUrl;

    @Override
    public List<PayDetail> pay(String payJournalNo, OrderInfo orderInfo, String userId) {
        com.epay.cu.etcpa.web.entity.OrderInfo orderInfoPO = new com.epay.cu.etcpa.web.entity.OrderInfo();
        BeanUtils.copyProperties(orderInfo, orderInfoPO);
        orderInfoPO.setStatus(OrderStatusEnum.INIT.code());
        // 保存订单信息
        orderInfoMapper.insert(orderInfoPO);
        // 保存支付流水
        PayJournal payJournalPO = new PayJournal();
        payJournalPO.setAmount(orderInfo.getAmount());
        payJournalPO.setMerOrderNo(orderInfo.getMerOrderNo());
        payJournalPO.setPayJournalNo(payJournalNo);
        payJournalPO.setStatus(PayJournalStatusEnum.INIT.code());
        payJournalMapper.insert(payJournalPO);
        // 调用账务根据userId查询余额 ...
        // 调用账务记账 ...
        // 保存记账流水
        PayDetail payDetail = new PayDetail();
        payDetail.setPayTool("001");
        payDetail.setFundSource("现金");
        payDetail.setAmount(orderInfo.getAmount());
        return Arrays.asList(payDetail);
    }

    @Override
    public OrderInfo getOrder(String merOrderNo) {
        com.epay.cu.etcpa.web.entity.OrderInfo orderInfoPO = orderInfoMapper.selectByPrimaryKey(merOrderNo);
        OrderInfo orderInfo = new OrderInfo();
        if (orderInfoPO != null) {
            BeanUtils.copyProperties(orderInfoPO, orderInfo);
        }
        logger.info("调用unipay的信息查看：{}", unipayUrl);
        return orderInfo;
    }
}
